
export function applyFormula(formula, variables)
{
    if (!formula) {
        return 0;
    }

    Object.keys(variables).forEach((variable) => {
        formula = formula.replace(variable, variables[variable]);
    });

    // todo - let's do this without eval
    return eval(formula);
}