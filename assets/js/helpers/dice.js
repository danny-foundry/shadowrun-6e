export function DiceConfig(wildDie = false, explodeSixes = false, twosGlitch = false, ignoreWoundPenalties = false)
{
    this.wildDie = wildDie;
    this.explodeSixes = explodeSixes;
    this.twosGlitch = twosGlitch;
    this.ignoreWoundPenalties = ignoreWoundPenalties;
}

export function splitPoolString(pool)
{
    let parts = [];
    let expr = /[+\-]/;
    let operation = '+';
    let symbol = '';

    while(symbol = expr.exec(pool)) {
        let val = pool.substring(0, pool.indexOf(symbol));
        pool = pool.substring(pool.indexOf(symbol) + 1);
        parts.push(`${operation}${val}`)
        operation = symbol;
    }
    parts.push(`${operation}${pool}`);

    return parts;
}

/**
 * @deprecated
 * @param pool
 * @param options
 * @returns {Promise<{glitch: boolean, successes: number, count: {"1": number, "2": number, "3": number, "4": number, "5": number, "6": number}, dice: *[]}>}
 */
export async function rollDice(pool, options = {})
{
    const results = await new Roll(`${pool}d6`).roll();

    let output = {
        successes: 0,
        glitch: false,
        count:{
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        dice: []
    };

    results.dice[0].results.forEach((dice) => {
        output.dice.push(dice.result);
        output.count[dice.result]++;
    });

    output.successes = output.count[5] + output.count[6];

    if(options.wildDie) {
        // treat dice[0] as the wild die
        //  a 1 cancels all 5's
        //  a 5 or 6 counts as 3 successes
        //  if using edge and 6's explode, re-roll 6 as normal dice
        if (output.dice[0] === 1) {
            output.successes -= output.count[5];
        }
        if (output.dice[0] >= 5) {
            output.successes += 2;
        }
    }

    if (options.explodeSixes && output.count[6] > 0) {
        options.wildDie = false;
        const reroll = await rollDice(output.count[6], options);

        output.dice = [...output.dice, ...reroll.dice];
        output.successes += reroll.successes;
    }

    let glitches = output.count[1];
    if (options.twosGlitch) {
        glitches += output.count[2];
    }
    if (glitches >= pool/2) {
        output.glitch = true;
    }
    return output;
}

export function renderRollInChat(result, text, pool, user, speaker, type, sound, options)
{
    let parts = [];

    result.dice.forEach((die, index) => {
        if (options.wildDie && index === 0) {
            parts.push(
                `<span class="die wild-die">${die}</span>`
            );
        } else {
            parts.push(
                `<span class="die">${die}</span>`
            );
        }
    });
    let content = parts.join('');

    if (result.glitch && result.successes === 0) {
        content += '<div style="color: red;"><span style="font-weight:bold;">CRITICAL</span> glitch</div>';
    } else if (result.glitch) {
        content += '<div style="color: red;">glitch</div>';
    }

    if (result.extra) {
        content += result.extra;
    }

    CONFIG.ChatMessage.documentClass.create({
        user: user,
        speaker: speaker,
        type: type,
        flavor: `${text}<br>\n${result.successes} successes!`,
        content: content,
        sound: sound,
    });
}

export async function rollDiceInChat(text, pool, user, speaker, type, sound, options = {})
{
    const result = await rollDice(pool, options);

    renderRollInChat(result, text, pool, user, speaker, type, sound, options);

    Users.instance.get(user).update({
        lastRoll: {
            text: text,
            pool: pool,
            type: type,
            sound: sound,
            options: options,
            result: result,
        }
    });
}

export async function parseRoll(pool, actor, options = {})
{
    let parts = [];

    splitPoolString(pool).forEach((part) => {
        let op = part[0];
        let value = part.substring(1);
        let name = '';

        if (isNaN(value)) {
            value = actor.getValue(value);
            const nameParts = part.split(".");
            name = `${nameParts[nameParts.length - 1]}`;
            name = `${value} (${name})`;

            if (op !== '+' || parts.length > 0) {
                name = `${op}${name}`;
            }
        } else {
            value = parseInt(value);
            name = `${op}${value}`;
        }

        parts.push({
            operator: op,
            pool: value,
            name: name,
        });
    });

    if (options.modifier) {
        let op = options.modifier >= 0 ? '+' : '-';
        let val = Math.abs(options.modifier);
        parts.push({
            operator: op,
            pool: val,
            name: `${op}${options.modifier}`,
        });
    }

    if (options.specialisation) {
        parts.push({
            operator: '+',
            pool: 2,
            name: '+2 (Specialisation)',
        });
    }

    if (options.expertise) {
        parts.push({
            operator: '+',
            pool: 3,
            name: '+3 (Expertise)',
        });
    }

    let woundPenalties = actor.data.data.condition_physical.penalty + actor.data.data.condition_stun.penalty;
    if (woundPenalties && !options.ignoreWoundPenalties) {
        parts.push({
            operator: '-',
            pool: woundPenalties,
            name: `${woundPenalties} (wounds)`,
        });
    }

    return {
        text: parts.map((part) => part.name),
        pool: parts.reduce((total, pool) => total + pool.pool, 0),
        parts: parts,
    };
}