import {DiceConfig, rollDiceInChat, splitPoolString, parseRoll} from "./dice.js";
import {formToObject} from "./objects.js";

export function messageBox(title, template, data = {})
{
    $.get(template, (content) => {
        let compiled = Handlebars.compile(content);
        Dialog.prompt({
            title: title,
            content: compiled(data),
            callback: () => {},
        });
    });
}

export function dialogForm(title, template, data = {})
{
    return new Promise((resolve, reject) => {
        $.get(template, async (content) => {
            let compiled = Handlebars.compile(content);
            await Dialog.prompt({
                title: title,
                content: compiled(data),
                callback: async (dialog) => {
                    resolve(formToObject($('form', dialog)));
                }
            });
        });
    });
}

export async function rollOptions(options)
{
    let optionsForm = await dialogForm('Roll options', '/systems/shadowrun-6e/assets/templates/dialogs/advanced-roll.html', { options: options });
    options.wildDie = optionsForm.wildDie === '1';
    options.explodeSixes = optionsForm.explodeSizes === '1';
    options.twosGlitch = optionsForm.twosGlitch === '1';
    options.ignoreWoundPenalties = optionsForm.ignoreWoundPenalties === '1';
    options.modifier = parseInt(optionsForm.modifier);
    options.specialisation = optionsForm.specialisation === '1';
    options.expertise = optionsForm.expertise === '1';

    return options;
}

export const bindings = {
    createButtons($doc, actor) {
        $doc.on('click', '[data-action="add-item"]', function() {
            let type = $(this).data('item-type');

            let data = {
                name: 'Untitled',
                type: type,
                // data: {
                //
                // }
            };

            actor.createEmbeddedDocuments("Item", [data]);
        })
    },
    editButtons($doc, actor) {
        $doc.find('[data-action="edit-item"]').click(function() {
            const itemId = $(this).data('item-id');
            actor.getEmbeddedCollection("Item").get(itemId).sheet.render(true);
        });
    },
    deleteButtons($doc, actor) {
        const self = this;

        $doc.find('[data-action="delete-item"]').click(async function() {
            const itemId = $(this).data('item-id');
            const name = actor.getEmbeddedCollection("Item").get(itemId).data.name;

            // todo - confirmation dialog
            const confirmed = true;
            // const confirmed = await self.confirm(`Delete '${name}'?`, 'systems/gurps-4/templates/dialogs/confirm.html', {
            //     name: name,
            //     action: 'delete',
            // });

            if (confirmed) {
                await actor.deleteOwnedItem(itemId);
            }
        });
    },
    rollButtons($doc, actor) {
        $doc.find('[data-action="roll"]').click(async function(ev) {
            let ignoreWoundPenalties = parseInt($(this).attr('data-wound-penalties')) === 0;
            let options = new DiceConfig(false, false, false, ignoreWoundPenalties);

            if (ev.shiftKey) {
                options = await rollOptions(options);
            }

            const pool = await parseRoll($(this).attr('data-pool'), actor, options);

            if (pool.pool < 1) {
                pool.pool = 1;
            }

            const rollName = $(this).attr('data-roll-name') || '';
            const rollText = `<div><strong>Rolling ${rollName}</strong></div>`;
            const rollComponentText = pool.text.map((text) => `<div>${text}</div>`).join('');
            const rollPoolText = `<hr/><div><strong>Pool: </strong>${pool.pool}</div>`;
            let optionsText = [];

            if (options.wildDie) {
                optionsText.push('<div style="font-style:italic">with a wild die</div>');
            }
            if (options.explodeSixes) {
                optionsText.push('<div style="font-style:italic">sixes explode</div>');
            }
            if (options.twosGlitch) {
                optionsText.push('<div style="font-style:italic">twos glitch</div>');
            }

            const rollOptionsText = optionsText.join("\n");
            const text = `
                ${rollText}
                ${rollComponentText}
                ${rollPoolText}
                ${rollOptionsText}
            `;

            rollDiceInChat(
                text,
                pool.pool,
                game.user.id,
                ChatMessage.getSpeaker({ actor: actor }),
                CONST.CHAT_MESSAGE_TYPES.IC,
                CONFIG.sounds.dice,
                options
            );
        });
    }
};