import {dialogForm, messageBox} from "./sheet.js";
import {renderRollInChat, rollDiceInChat} from "./dice.js";

function rerollDice(roll, count, wild = false) {
    // first re-roll the wild die if applicable, then re-roll the lowest
    alert('not implemented');
    return roll;
}

function addOne(roll, count, wild = false) {
    // add one to the wild die if applicable, then add to 4's, then multi-add to 3's
    alert('not implemented');
    return roll;
}

function rerollFailures(initialPool, roll, options) {
    let pool = roll.count[1] + roll.count[2] + roll.count[3] + roll.count[4];

    let glitches = 0;
    let firstRoll = true;
    let rerollingWildDie = options.wildDie && roll.dice[0] < 5;
    let wildDie = 0;
    let reroll = { ...roll };

    reroll.count[1] = 0;
    reroll.count[2] = 0;
    reroll.count[3] = 0;
    reroll.count[4] = 0;

    while(pool > 0) {
        let result = new Roll(`${pool}d6`).roll();
        pool = 0;

        result.dice[0].results.forEach((dice, index) => {
            let isWildDie = rerollingWildDie && index === 0 && firstRoll;
            if (isWildDie) {
                wildDie = dice.result;
            }

            reroll.dice.push(dice.result);
            reroll.count[dice.result]++;

            if ((dice.result === 5 && wildDie !== 1) || dice.result === 6) {
                reroll.successes += isWildDie ? 3 : 1;
            }
            if (dice.result === 6 && options.explodeSizes) {
                pool ++;
            }

            if (firstRoll) {
                if (dice.result === 1 || (dice.result === 2 && options.twosGlitch)) {
                    glitches ++;
                }
            }
        });
        firstRoll = false;
    }

    reroll.glitch = glitches >= initialPool / 2;

    return reroll;
}

export function bindGlobalFunctions() {
    window.sr6PostRollEdge = async function() {
        const data = Users.instance.current.data.lastRoll;
        if (!data) {
            // todo - display a message
            messageBox('No previous roll', '/systems/shadowrun-6e/assets/templates/dialogs/no-previous-roll.html', {});
            return;
        }

        const roll = data.result;
        let optionsForm = await dialogForm('Post roll edge', '/systems/shadowrun-6e/assets/templates/dialogs/post-roll.html', {
            disableRerollFailures: roll.glitch,
        });
        let reroll = roll;
        let cost = 0;

        switch(optionsForm.edge_action) {
            case 'reroll_dice': // 1 edge
                reroll = rerollDice(roll, parseInt(optionsForm.qty), optionsForm.reroll_wild === '1');
                reroll.extra = `<div>Rerolled ${optionForm.qty} dice</div>`;
                cost = parseInt(optionsForm.qty);
                break;
            case 'add_one': // 2 edge
                reroll = addOne(roll, parseInt(optionsForm.qty), optionsForm.add_to_wild === '1');
                reroll.extra = `<div>Added 1 to ${optionForm.qty} dice</div>`;
                cost = parseInt(optionsForm.qty) * 2;
                break;
            case 'add_hit': // 3 edge
                reroll.successes += parseInt(optionsForm.qty);
                reroll.extra = `<div>Added ${optionsForm.qty} hits</div>`;
                cost = parseInt(optionsForm.qty) * 3;
                break;
            case 'reroll_failed': // 4 edge
                if (roll.glitch) {
                    messageBox('Bad edge action', '/systems/shadowrun-6e/assets/templates/dialogs/cannot-reroll-failures-after-glitch.html')
                    return;
                }
                reroll = rerollFailures(data.pool, roll, data.options);
                reroll.extra = `<div>Rerolled failures</div>`;
                cost = 4;
                break;
            default:
                messageBox('No edge action selected', '/systems/shadowrun-6e/assets/templates/dialogs/no-action-selected.html');
                return;
        }

        messageBox('Spend edge', '/systems/shadowrun-6e/assets/templates/dialogs/spend-edge.html', { cost: cost });
        renderRollInChat(reroll, data.text, data.pool, data.user, data.speaker, data.type, data.sound, data.options);
    }
}