export function getItemsByType(object, type, sorted = true) {
    let items = object.items.filter((item) => item.type === type);
    if (sorted) {
        return items.sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        })
    }
    return items;
}

export function getItemsByTypes(object, types, sorted = true) {
    let items = object.items.filter((item) => types.indexOf(item.type) !== -1);
    if (sorted) {
        return items.sort((a, b) => {
            return b.name > a.name ? -1 : 1;
        })
    }
    return items;
}