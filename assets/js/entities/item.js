export default class ShadowrunItem extends Item {
    // object setup - before items and effects
    prepareBaseData() {
        const equippable = ["weapon", "explosives", "armour", "commlink", "cyberdeck", "equipment"];

        if (this.data.type === 'skill') {
            // todo set modifier and value IF it's linked to a character
        }

        if (equippable.indexOf(this.data.type) && !this.data.data.equipped) {
            this.data.data.equipped = true;
        }
    }

    // set any computed attributes, including from items and effects
    prepareDerivedData() {

    }

    getEffects() {
        if (this.data.type === 'cyberware') {
            let effects = this.data.data.effects;
            let essence = parseFloat(this.data.data.essence);
            let grade = this.data.data.grade;

            if (!this.data.data.active) {
                effects = '';
            }

            switch(grade) {
                case 'used':
                    essence = essence * 1.1;
                    break;
                case 'alpha':
                    essence = essence * 0.8;
                    break;
                case 'beta':
                    essence = essence * 0.7;
                    break;
                case 'delta':
                    essence = essence * 0.5;
                    break;
            }

            if (effects) {
                effects += "\n";
            }
            return effects + 'attributes.Essence=-' + essence;
        }
        return this.data.data.effects || '';
    }
}
