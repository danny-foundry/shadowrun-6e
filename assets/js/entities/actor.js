import {object_get} from "../helpers/objects.js";
import {getItemsByType} from "../helpers/actor.js";
import {applyFormula} from "../helpers/math.js";

export default class ShadowrunActor extends Actor
{
    _calculateKarmaAvailable() {
        if (typeof this.data.data.karma !== 'object') {
            this.data.data.karma = {
                earnt: 0,
                spent: 0,
                available: 0
            };
        }
        this.data.data.karma.available = this.data.data.karma.earnt - this.data.data.karma.spent;
    }

    // object setup - before items and effects
    prepareBaseData() {
        if (this.data.type === 'character') {
            this._calculateKarmaAvailable();
        }
    }

    _calculateAttributeDerivedValues() {
        Object.keys(this.data.data.attributes).forEach((attr) => {
            let modifier = this.getModifier(`attributes.${attr}`);
            let raw_modifier = this.getModifier(`attributes.${attr}`, false);
            let value = this.getValue(`attributes.${attr}`);
            if (modifier >= 0) {
                modifier = `+${modifier}`;
                raw_modifier = `+${raw_modifier}`;
            }

            this.data.data.attributes[attr].modifier = modifier;
            this.data.data.attributes[attr].raw_modifier = raw_modifier;
            this.data.data.attributes[attr].total = value;
        });
    }

    _calculateWoundPenalties() {
        let penaltyBoundary = 3;
        if (this.data.type === 'grunt') {
            penaltyBoundary = 2;
        }

        const physicalDamage = parseInt(this.data.data.condition_physical.value) + parseInt(this.data.data.condition_physical.agony);
        this.data.data.condition_physical.penalty = -Math.floor(physicalDamage / penaltyBoundary);

        // check for the last row being filled (and not having a multiple of {boundary} hitboxes)
        if (physicalDamage === this.data.data.condition_physical.max && physicalDamage % penaltyBoundary > 0) {
            this.data.data.condition_physical.penalty --;
        }

        if (this.data.type !== 'grunt') {
            const stunDamage = parseInt(this.data.data.condition_stun.value) + parseInt(this.data.data.condition_stun.agony);
            this.data.data.condition_stun.penalty = -Math.floor(stunDamage / penaltyBoundary);
            if (stunDamage === this.data.data.condition_stun.max && stunDamage % penaltyBoundary > 0) {
                this.data.data.condition_stun.penalty --;
            }
        }
    }

    _setMaxEdge() {
        this.data.data.edge.max = this.getValue('attributes.Edge');
    }

    _calculateInitiative() {
        this.data.data.initiative.calculated = this.getValue('attributes.Reaction') + this.getValue('attributes.Intuition');
        this.data.data.initiative.total = this.data.data.initiative.calculated + this.data.data.initiative.modifier;
    }

    _calculateDerivedCharacteristics() {
        this.data.data.dodge = this.getValue('attributes.Reaction') + this.getValue('attributes.Intuition');
        this.data.data.composure = this.getValue('attributes.Willpower') + this.getValue('attributes.Charisma');
        this.data.data.judge = this.getValue('attributes.Willpower') + this.getValue('attributes.Intuition');
        this.data.data.memory = this.getValue('attributes.Logic') + this.getValue('attributes.Intuition');
        this.data.data.lift = this.getValue('attributes.Body') + this.getValue('attributes.Willpower');
        this.data.data.soak_damage = this.getValue('attributes.Body');

        if (this.data.data.settings.drain_attribute) {
            this.data.data.soak_drain = this.getValue('attributes.Willpower') + this.getValue(`attributes.${this.data.data.settings.drain_attribute}`);
        }

        if (!this.data.data.defenseRating || typeof this.data.data.defenseRating !== 'object') {
            this.data.data.defenseRating = { value : 0 };
        }

        this.data.data.defenseRating.value = this.getValue('attributes.Body');
        this.items.forEach((item) => {
            if (item.data.data.defense && (item.data.data.equipped === undefined || item.data.data.equipped)) {
                this.data.data.defenseRating.value += parseInt(item.data.data.defense);
            }
        });
    }

    _calculateSpiritAttributes() {
        Object.keys(this.data.data.attributes).forEach((attr) => {
            this.data.data.attributes[attr].value = applyFormula(this.data.data.attributes[attr].calc, {
                'F' : this.data.data.force,
            });
            if (this.data.data.attributes[attr].value < 1) {
                this.data.data.attributes[attr].value = 1;
            }

            let modifier = this.getModifier(`attributes.${attr}`);
            let raw_modifier = this.getModifier(`attributes.${attr}`, false);
            let value = this.getValue(`attributes.${attr}`);
            if (modifier >= 0) {
                modifier = `+${modifier}`;
                raw_modifier = `+${raw_modifier}`;
            }

            this.data.data.attributes[attr].modifier = modifier;
            this.data.data.attributes[attr].raw_modifier = raw_modifier;
            this.data.data.attributes[attr].total = value;
        });

        if (!this.data.data.astral_initiative) {
            this.data.data.astral_initiative = { calc: '', dice: 0, modifier: 0 };
        }

        this.data.data.initiative.calculated = applyFormula(this.data.data.initiative.calc, {
            'F' : this.data.data.force,
        });
        this.data.data.astral_initiative.calculated = applyFormula(this.data.data.astral_initiative.calc, {
            'F' : this.data.data.force,
        });
        this.data.data.defenseRating.value = applyFormula(this.data.data.defenseRating.calc, {
            'F' : this.data.data.force,
        });
    }

    _setSpiritSkillRanks() {

        getItemsByType(this, 'skill').forEach((skill) => {
            if (!this.isOwner && !game.user.isGM) {
                return;
            }
            // this shit causes an infinite update (but only on live)

            skill.update({
                data: {
                    ranks: this.data.data.force,
                }
            });
        });
    }

    // set any computed attributes for this entity, including from items and effects
    prepareDerivedData() {
        if (this.data.type === 'character') {
            this._calculateAttributeDerivedValues();
            this._calculateWoundPenalties();
            this._setMaxEdge();
            this._calculateInitiative();
            this._calculateDerivedCharacteristics();
        } else if (this.data.type === 'grunt') {
            this._calculateAttributeDerivedValues();
            this._calculateWoundPenalties();
            this._calculateInitiative();
            this._calculateDerivedCharacteristics();
        } else if (this.data.type === 'vehicle') {
            this.data.data.speed.penalty = Math.floor(this.data.data.speed.current / this.data.data.speed.interval);
        } else if (this.data.type === 'spirit') {
            this._calculateWoundPenalties();
            this._calculateInitiative();
            this._calculateDerivedCharacteristics();

            // broken on live
            //this._setSpiritSkillRanks();
            this._calculateSpiritAttributes();
        }
    }

    getCharacteristic(characteristic) {
        let keys = characteristic.split(".");
        if (keys[0] === 'skills') {
            if (this.data.type === 'spirit') {
                return { value: this.data.data.force };
            }

            let items = getItemsByType(this, 'skill').filter((skill) => {
                return skill.name === keys[1];
            });

            if (!items.length) {
                return { modifier: 0, value: 0 };
            }

            return { value: items[0].data.data.ranks, modifier: items[0].data.data.modifier || 0 };
        }

        if (keys.length === 1) {
            switch(keys[0]) {
                case 'force':
                    return { value: this.data.data.force };
            }
        }
        return object_get(this.data.data, keys);
    }

    getModifier(characteristic, rounded = true) {
        let modifier = 0;

        this.items.forEach((item) => {
            const effects = item.getEffects();
            if (!effects) {
                return;
            }

            effects.split("\n").forEach((effect) => {
                let position = effect.indexOf('=');
                let name = effect.substring(0, position);
                let mod = effect.substring(position + 1);

                if (name !== characteristic) {
                    return;
                }
                modifier += parseFloat(mod);
            });
        });

        if (rounded) {
            modifier = Math.floor(modifier);
        }
        return modifier;
    }

    getValue(characteristic) {
        let data = this.getCharacteristic(characteristic);
        let modifier = this.getModifier(characteristic);

        return parseInt(data.value) + parseInt(modifier);
    }
}