import {getItemsByType, getItemsByTypes} from "../helpers/actor.js";
import {bindings} from "../helpers/sheet.js";

export default class ShadowrunGruntSheet extends ActorSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.actor.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/shadowrun-6e/assets/templates/character-limited.html";
        }
        // use a full editable sheet
        return "systems/shadowrun-6e/assets/templates/grunt.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        let gearTypes = {
            "weapon": "Weapon",
            "explosives": "Explosives",
            "armour": "Armour",
            "commlink": "Commlink",
            "cyberdeck": "Cyberdeck",
            "equipment": "Equipment",
            "identity": "Identities",
            "software": "Software",
            "vehicle": "Vehicle/Drone",
            "metamagic": "Metamagics",
            "spell": "Spells",
        };

        let gearByType = {};
        Object.entries(gearTypes).forEach((entry) => {
            gearByType[entry[0]] = getItemsByType(this.actor, entry[0]);
        });

        return {
            actor: this.actor.data,
            skills: getItemsByType(this.actor, 'skill'),
            qualities: getItemsByType(this.actor, 'quality'),
            contacts: getItemsByType(this.actor, 'contact'),
            cyberware: getItemsByType(this.actor, 'cyberware'),
            gear: getItemsByTypes(this.actor, Object.keys(gearTypes)),
            isEditable: this.isEditable,
            gearByType,
            gearTypes,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);
        if (!this.isEditable) {
            return;
        }

        bindings.createButtons($doc, this.actor);
        bindings.editButtons($doc, this.actor);
        bindings.deleteButtons($doc, this.actor);
        bindings.rollButtons($doc, this.actor);
    }
}