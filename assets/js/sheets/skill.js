export default class SkillSheet extends ItemSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.object.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/shadowrun-6e/assets/templates/item-limited.html";
        }
        // use a full editable sheet
        return "systems/shadowrun-6e/assets/templates/skill.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        const actor = this.object.actor;
        const isSpirit = actor && actor.data.type === 'spirit';

        return {
            item: this.object.data,
            actor: this.object.actor,
            hasRanks: actor && !isSpirit,
            hasSpecialisation: actor,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);
        
        // bind any dom listeners that apply to all sheets
        if (!this.isEditable) {
            return;
        }

        // bind any dom listeners that apply to editable sheets
    }
}