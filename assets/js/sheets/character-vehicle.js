import {getItemsByType} from "../helpers/actor.js";
import {bindings} from "../helpers/sheet.js";

export default class VehicleCharacterSheet extends ActorSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.object.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/shadowrun-6e/assets/templates/character-limited.html";
        }
        // use a full editable sheet
        return "systems/shadowrun-6e/assets/templates/character-vehicle.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        const characters = Actors.instance.entries.filter((actor) => actor.data.type === 'character');

        return {
            actor: this.actor.data,
            characters: characters,
            isEditable: this.isEditable,
            weapons: getItemsByType(this.actor,'weapon'),
            autosofts: getItemsByType(this.actor, 'autosoft')
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);

        // bind any dom listeners that apply to all sheets
        if (!this.isEditable) {
            return;
        }

        // bind any dom listeners that apply to editable sheets
        bindings.createButtons($doc, this.actor);
        bindings.editButtons($doc, this.actor);
        bindings.deleteButtons($doc, this.actor);
        bindings.rollButtons($doc, this.actor);
    }
}