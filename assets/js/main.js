import ShadowrunActor from "./entities/actor.js";
import ShadowrunItem from "./entities/item.js";
import ShadowrunCharacterSheet from "./sheets/character.js";
import ShadowrunGruntSheet from "./sheets/grunt.js";
import ShadowrunSpiritSheet from "./sheets/spirit.js";
import SkillSheet from "./sheets/skill.js";
import ContactSheet from "./sheets/contact.js";
import CyberwareSheet from "./sheets/cyberware.js";
import QualitySheet from "./sheets/quality.js";
import PowerSheet from "./sheets/power.js";
import IdentitySheet from "./sheets/identity.js";
import VehicleSheet from "./sheets/vehicle.js";
import VehicleCharacterSheet from "./sheets/character-vehicle.js";
import GearSheet from "./sheets/gear.js";
import {bindGlobalFunctions} from "./helpers/global.js";

bindGlobalFunctions();

Hooks.once('init', () => {
    CONFIG.Actor.documentClass = ShadowrunActor;
    CONFIG.Item.documentClass = ShadowrunItem;

    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("shadowrun", ShadowrunCharacterSheet, {
        types: ["character"],
        makeDefault: true,
        label: "Character",
    });
    Actors.registerSheet("shadowrun", ShadowrunGruntSheet, {
        types: ["grunt"],
        label: "Grunt",
    });
    Actors.registerSheet("shadowrun", ShadowrunSpiritSheet, {
        types: ["spirit"],
        label: "Spirit",
    });
    Actors.registerSheet("shadowrun", VehicleCharacterSheet, {
        types: ["vehicle"],
        label: "Vehicle",
    });

    Items.unregisterSheet("core", ItemSheet);

    Items.registerSheet("shadowrun", ContactSheet, {
        types: ["contact"],
        label: "Contact"
    });

    Items.registerSheet("shadowrun", CyberwareSheet, {
        types: ["cyberware"],
        label: "Cyberware"
    });

    Items.registerSheet("shadowrun", SkillSheet, {
        types: ["skill"],
        label: "Skill"
    });

    Items.registerSheet("shadowrun", QualitySheet, {
        types: ["quality"],
        label: "Quality"
    });

    Items.registerSheet("shadowrun", PowerSheet, {
        types: ["power"],
        label: "Power"
    });

    Items.registerSheet("shadowrun", IdentitySheet, {
        types: ["identity"],
        label: "Identity"
    });

    Items.registerSheet("shadowrun", VehicleSheet, {
        types: ["vehicle"],
        label: "Vehicle"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["autosoft"],
        label: "Autosoft"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["metamagic"],
        label: "Metamagic"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["spell"],
        label: "Spell"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["weapon"],
        label: "Weapon"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["explosives"],
        label: "Explosives"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["armour"],
        label: "Armour"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["commlink"],
        label: "Commlink"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["cyberdeck"],
        label: "Cyberdeck"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["equipment"],
        label: "Equipment"
    });

    Items.registerSheet("shadowrun", GearSheet, {
        types: ["software"],
        label: "Software"
    });

    loadTemplates([
        'systems/shadowrun-6e/assets/templates/character/attributes.html',
        'systems/shadowrun-6e/assets/templates/character/attribute.html',
        'systems/shadowrun-6e/assets/templates/character/contacts.html',
        'systems/shadowrun-6e/assets/templates/character/condition-monitors.html',
        'systems/shadowrun-6e/assets/templates/character/cyberware.html',
        'systems/shadowrun-6e/assets/templates/character/gear.html',
        'systems/shadowrun-6e/assets/templates/character/gear-by-type.html',
        'systems/shadowrun-6e/assets/templates/character/knowledges.html',
        'systems/shadowrun-6e/assets/templates/character/misc.html',
        'systems/shadowrun-6e/assets/templates/character/qualities.html',
        'systems/shadowrun-6e/assets/templates/character/settings.html',
        'systems/shadowrun-6e/assets/templates/character/skills.html',
        'systems/shadowrun-6e/assets/templates/grunt/condition-monitors.html',
        'systems/shadowrun-6e/assets/templates/spirit/settings.html',
        'systems/shadowrun-6e/assets/templates/spirit/attributes.html',
        'systems/shadowrun-6e/assets/templates/spirit/attribute.html',
        'systems/shadowrun-6e/assets/templates/spirit/powers.html',
        'systems/shadowrun-6e/assets/templates/spirit/skills.html',
        'systems/shadowrun-6e/assets/templates/vehicle/autosofts.html',
        'systems/shadowrun-6e/assets/templates/vehicle/weapons.html',
        'systems/shadowrun-6e/assets/templates/list-items/autosoft.html',
        'systems/shadowrun-6e/assets/templates/list-items/armour.html',
        'systems/shadowrun-6e/assets/templates/list-items/commlink.html',
        'systems/shadowrun-6e/assets/templates/list-items/cyberdeck.html',
        'systems/shadowrun-6e/assets/templates/list-items/equipment.html',
        'systems/shadowrun-6e/assets/templates/list-items/explosives.html',
        'systems/shadowrun-6e/assets/templates/list-items/identity.html',
        'systems/shadowrun-6e/assets/templates/list-items/metamagic.html',
        'systems/shadowrun-6e/assets/templates/list-items/spell.html',
        'systems/shadowrun-6e/assets/templates/list-items/software.html',
        'systems/shadowrun-6e/assets/templates/list-items/vehicle.html',
        'systems/shadowrun-6e/assets/templates/list-items/weapon.html',
    ]);

    Handlebars.registerHelper('field_id', function(entity, field) {
        return entity._id + '-' + field;
    });

    Handlebars.registerHelper('json', function(val) {
        console.log(val);
        return JSON.stringify(val);
    });

    Handlebars.registerHelper('partial', function(value, opt) {
        return Handlebars.partials['systems/shadowrun-6e/assets/templates/' + value](opt.data.root);
    });

    // modifier, raw_modifier, total
    Handlebars.registerHelper("render_attribute", function(actor, attribute, opt) {
        let data = {
            attribute: attribute,
            value: actor.data.attributes[attribute],
            isEditable: opt.data.root.isEditable,
        };

        return Handlebars.partials['systems/shadowrun-6e/assets/templates/character/attribute.html'](data);
    });

    Handlebars.registerHelper("render_spirit_attribute", function(actor, attribute, opt) {
        let data = {
            attribute: attribute,
            value: actor.data.attributes[attribute],
            isEditable: opt.data.root.isEditable,
        };

        return Handlebars.partials['systems/shadowrun-6e/assets/templates/spirit/attribute.html'](data);
    });

    Handlebars.registerHelper("render_gear", function(actor, item, opt) {
        return Handlebars.partials[`systems/shadowrun-6e/assets/templates/list-items/${item.type}.html`]({
            actor: actor,
            item: item
        });
    });
});