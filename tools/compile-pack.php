<?php

$compiled = dirname(__DIR__).'/packs/';
$decompiled = dirname(__DIR__).'/data/';

$pack = $argv[1] ?? '';
if (!$pack) {
    echo "\e[31mUsage: php decompile-pack.php {packName}\e[0m\n";
    exit;
}

$source = $decompiled.$pack.'.json';
$destination = $compiled.$pack.'.db';
if (!file_exists($source)) {
    echo "\e[31mPack not found {$source}\e[0m\n";
    exit;
}

if (file_exists($destination)) {
    unlink($destination);
}

$idCache = [];
$id = 999;
$data = [];

$data = json_decode(file_get_contents($source), true);
if (!$data) {
    echo "\e[31mFailed to load source data\e[0m\n";
    echo "\e[31m".json_last_error_msg()."\e[0m\n";
    exit;
}

$handle = fopen($destination, 'w');
foreach ($data as $row) {
    if (!isset($row['_id']) || in_array($row['_id'], $idCache)) {
        $row['_id'] = base_convert($id, 10, 36);
        $id ++;
    }
    fwrite($handle, json_encode($row)."\n");
}
