<?php

$compiled = dirname(__DIR__).'/packs/';
$decompiled = dirname(__DIR__).'/data/';

$pack = $argv[1] ?? '';
if (!$pack) {
    echo "\e[31mUsage: php decompile-pack.php {packName}\e[0m\n";
    exit;
}

$source = $compiled.$pack.'.db';
$destination = $decompiled.$pack.'.json';
if (!file_exists($source)) {
    echo "\e[31mPack not found {$source}\e[0m\n";
    exit;
}

if (file_exists($destination)) {
    unlink($destination);
}

$data = [];
$handle = fopen($source, 'r');
while($line = fgets($handle)) {
    $row = json_decode($line, true);
    $data[] = $row;
}
file_put_contents($destination, json_encode($data, JSON_PRETTY_PRINT));